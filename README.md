# Electron boilerplate

Some boilerplate for writing desktop applications using Javascript.

## Usage:

When you clone this project, edit package.json to set
  1. the name of the project;
  2. the author of the project;
  3. the link to the repository;
  4. the home page, if any; and
  5. the license.

To install dependencies, run

```bash
npm install
```

Dependencies include electron and electron-packager. The latter is invoked using the npm script

```bash
npm run-script package
```

which creates binaries for all supported platforms in the *build* folder.

The scripts

```bash
npm run-script clean
```

and

```bash
npm run-script clean-dev
```

clean out the build and node_modules folders respectively. The *start* and *test* commands run as one would expect; Jasmine drives unit tests. No framework libraries have been included so far.


