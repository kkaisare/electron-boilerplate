const {app, BrowserWindow} = require('electron');

// Keep a global reference of the
// window object. If you don't the
// window will be automatically closed
// when the Javascript object is garbage
// collected.
let win;

function createWindow() {
	let browserDimensions = {
		width: 800,
		height: 600
	};
	let indexFile = `file://${__dirname}/src/index.html`; 
	
	win = new BrowserWindow(browserDimensions);
	win.loadURL(indexFile);

	win.webContents.openDevTools();

	win.on('closed', () => {
		win = null;
	});
}
app.on('ready', createWindow);

// On macOS, it is common to recreate a
// window in the app when the dock icon
// is clicked and there are no other
// windows open. 
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (win === null) {
		createWindow();
	}
});